#include <iostream>
#include <string>

int main()
{
    std::string first = "170", second = "5420";

    std::cout << first + second << "\n";
    std::cout << first.length() << "\n";
    std::cout << second.length() << "\n";

    std::cout << first.front() << "\n";
    std::cout << second.back() << "\n";

    return 0;
}



